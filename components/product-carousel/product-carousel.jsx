import styles from "./product-carousel.module.scss";
import SwiperCore, { Pagination, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

SwiperCore.use([Pagination, A11y]);

const ProductCarousel = ({ images, alt }) => {
  return (
    <div className={styles.productCarousel}>
      <Swiper modules={[Pagination, A11y]} slidesPerView={1} pagination={{ clickable: true }}>
        {images.map((image) => (
          <SwiperSlide key={image}><img src={image} style={{maxWidth: "500px", width: "100%"}} alt={alt} /></SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default ProductCarousel;
