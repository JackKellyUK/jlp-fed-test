import styles from "./product-specification-item.module.scss";

const ProductSpecificationItem = ({ name, value }) => {
    return (
        <li className={styles.item}>
            <div>{name}</div>
            <div>{value}</div>
        </li>
        );
    };
  
export default ProductSpecificationItem;
  