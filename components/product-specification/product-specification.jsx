import ProductSpecificationItem from './product-specification-item/product-specification-item';
import styles from "./product-specification.module.scss";

const ProductSpecification = ({ data }) => {
    return (
        <ul className={styles.list}>
            {data.attributes.map((item) => (
                <ProductSpecificationItem key={item.name} name={item.name} value={item.value} />
            ))}
        </ul>
        );
    };
  
export default ProductSpecification;