import React, { useState } from "react"
import styles from "./product-information.module.scss";
import DOMPurify from 'isomorphic-dompurify';

const ProductDescription = ({ description, limit }) => {
    const [showAll, setShowAll] = useState(false);
    return (
      <div>
        {description.length > limit ? (
          <div>
            {showAll ? (
              <div>
                <div dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(description) }}></div>
                <br />
                <button className={styles.button} onClick={() => setShowAll(false)}>
                  Read less
                </button>
              </div>
            ) : (
              <div>
                <div dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(description).substring(0, limit).concat("...") }}></div>
                <br />
                <button className={styles.button} onClick={() => setShowAll(true)}>
                  Read more
                </button>
              </div>
            )}
          </div>
        ) : (
          description
        )}
      </div>
    );
  };
  export default ProductDescription;