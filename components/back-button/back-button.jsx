import { useRouter } from 'next/router'

const BackButton = () => {
  const router = useRouter()

  return (
    <div aria-label="Go back" onClick={() => router.back()}>
      <img alt="Arrow" src="/arrow.png" />
    </div>
  );
}

export default BackButton;