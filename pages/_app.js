import Layout from "../components/layout/layout";
import "../styles/globals.scss";
import 'swiper/swiper-bundle.css';

function MyApp({ Component, pageProps }) {
  return (
    <Layout type="netherworld">
      <Component {...pageProps} />
    </Layout>
  );
}

export default MyApp;
