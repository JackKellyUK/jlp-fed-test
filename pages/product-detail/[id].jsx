import Head from "next/head";
import ProductCarousel from "../../components/product-carousel/product-carousel";
import ProductInformation from "../../components/product-information/product-information";
import ProductSpecification from "../../components/product-specification/product-specification";
import BackButton from '../../components/back-button/back-button';
import GetSymbolFromCurrency from 'currency-symbol-map';
import styles from './product-detail.module.scss';


export async function getServerSideProps(context) {
  const id = context.params.id;
  const file = require("../../mockData/data2.json");
  const data = file.detailsData.find(obj => {
    return obj.productId === id
  });

  return {
    props: { data },
  };
}

const ProductDetail = ({ data }) => {
  return (
    <div>
      <Head>
        <title>JL &amp; Partners | {data.title}</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <h1 className={styles.header}>{data.title} <span className={styles.back}><BackButton /></span></h1>
      <div className={styles.container}>

        <div className={styles.carousel}>
          <ProductCarousel images={data.media?.images.urls} alt={data.media?.images.altText} />
        </div>

        <div className={styles.divider}></div>

        <div className={styles.price}>
          <h2>{GetSymbolFromCurrency(data.price.currency)}{data.price?.now}</h2>
          <div style={{color: "#8c190f"}}>{data.displaySpecialOffer}</div>
          <div>{data.additionalServices?.includedServices}</div>
        </div>

        <div className={styles.description}>
          <div className={styles.information}>
            <h2>Product information</h2>
            <p style={{margin: "0"}}>Product code: {data.code}</p>
            
            <div className={styles.readmore}>
              <ProductInformation description={data.details.productInformation} limit={150}/>
            </div>
          </div>

          <div className={styles.specification}>
            <h2>Product specification</h2>
            <ProductSpecification data={data.details?.features[0]} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
