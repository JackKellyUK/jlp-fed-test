import Head from "next/head";
import Link from "next/link";
import styles from "./index.module.scss";
import getSymbolFromCurrency from 'currency-symbol-map';

export async function getServerSideProps() {
  const data = require("../mockData/data.json");
  return {
    props: {
      data: data,
    },
  };
}

const Home = ({ data }) => {
  let items = data.products;
  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div>
        <h1 className={styles.header}>Dishwashers ({items.length})</h1>
        <div className={styles.content}>
          {items.map((item) => (
            <Link
              key={item.productId}
              href={{
                pathname: "/product-detail/[id]",
                query: { id: item.productId },
              }}
            >
              <a className={styles.link}>
                <div className={styles.item}>
                  <img src={item.image} alt="" style={{ width: "100%" }} />
                  <div>{item.title}</div>
                  <div className={styles.price}>{getSymbolFromCurrency(item.price.currency)}{item.price.now}</div>
                </div>
              </a>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;